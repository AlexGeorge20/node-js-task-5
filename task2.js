const { response } = require('express');
const {app}= require('./app');
const { personRouter } = require('./route/person.route');

app.use(personRouter)
app.listen(8080,()=>{
    console.log('App is running');
    
})
