
const {db} = require ("../database")
const ObjectId = require('mongodb').ObjectId; 

//LIST items
async function getPersons(){
 let retresult= await db.find({}).toArray()
console.log("REsult", retresult);
return retresult;
}

//GET id
async function personCheckIdService(id){
  try{
   
  let o_id = new ObjectId(id);
  let retresult= await db.find({_id:o_id}).toArray()
console.log("REsult",retresult);
return ("ID FOUND", retresult);
}catch(err){
  console.log("ID NOT FOUND");
  return "ID NOT FOUND";
}
}
//DELETE Id
async function personDeleteIdService(id){
   try{ 
     console.log("Params", id);
    let o_id = new ObjectId(id);
    let delId = await db.deleteOne({_id:o_id})
    return delId;
}
catch(err){
  console.log("ID to be Deleted not found");
  return "ID to be Deleted not found"
}
}
//CREATE Id,name
async function personCreateService(name){
    console.log("BOdY",name); 
let insertItem=await db.insertOne({name:`${name}`})
// return insertItem;
return "INSertEd"+ " " + `Name: ${name}`+ " " + "ID:"+insertItem.insertedId;
}
//UPDATE name
async function personUpdateService(id,name){
try{
    console.log("bodyname",name);
   
    let o_id = new ObjectId(id);
    let retresult=  await db.find({_id:o_id}).toArray()
  console.log("REsult", retresult[0].name);

db.updateOne({name:retresult[0].name},{$set:{name:`${name}`}})
return 'UPDATED';
}
catch(err){
console.log("ID Not Found");
return "ID Not Found";
}
}
module.exports={personListService: getPersons, personCheckIdService, personDeleteIdService,personCreateService,personUpdateService}
    
