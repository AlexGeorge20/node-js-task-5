const Logger = require("nodemon/lib/utils/log");
const { personListService, personCheckIdService, personDeleteIdService, personCreateService, personUpdateService } = require("../service/person.service");

const personListController= async(req,res)=>{
    const temp =await personListService()
    console.log("READ",temp);
    res.send(temp)
}

const personCheckIdControler=async(req,res)=>{
    let id= req.params.id;
    console.log(id);
    const response= await personCheckIdService(id);
    res.send(response)
}

const personDeleteIdControler=async(req,res)=>{  
    const id= req.params.id;
    const response=await personDeleteIdService(id);
    console.log("DELreSPOnse",response);
    res.send(response)
  }
const personCreateController=async(req,res)=>{  
    const newName= req.body.newName;
    // console.log("REQname",newName);
    const response=await personCreateService(newName);
    console.log("reSPOnse",response);
    // res.send("INSertEd"+ " " + `name: ${newName}`+ " " + "ID:"+response.insertedId)
    res.send(response)
}  

const personUpdateController=async(req,res)=>{
       const newName=req.body.newName;
    //    console.log('bodyname',newName);
       const id=req.params.id;
       const response=await personUpdateService(id,newName)
       console.log("UpdatedRESponse",response);
       res.send(response)
         
}

module.exports={personListController,personCheckIdControler,personDeleteIdControler,personCreateController,personUpdateController}
